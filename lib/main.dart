import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'dashboard.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  // SystemChrome.setEnabledSystemUIOverlays([]); //fullscreen
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  runApp(Main());
}

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'สถานการณ์โรค COVID-19',
      home: DashboardPage(),
      theme: ThemeData(
          primaryColor: Colors.teal,
          accentColor: Colors.teal,
          fontFamily: 'Kanit'),
    );
  }
}
